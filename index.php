<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<title>Calculadora de Perks - Firefall Brasil</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/tooltip-line.css" />
<body>
<div id="wrapper">
	<div id="banner">
		<div class="wrap">
			<div class="cldisp_d dreadnaught"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Dreadnaught</span></div>
			<div class="cldisp_d arsenal"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Arsenal</span></div>
			<div class="cldisp_d rhino"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Rhino</span></div>
			<div class="cldisp_d mammoth"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Mammoth</span></div>

			<div class="cldisp_a assault"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Assault</span></div>
			<div class="cldisp_a tigerclaw"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Tigerclaw</span></div>
			<div class="cldisp_a firecat"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Firecat</span></div>

			<div class="cldisp_b biotech"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Biotech</span></div>
		</div>
		<div class="wrap">
			<div class="cldisp_b recluse"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Recluse</span></div>
			<div class="cldisp_b dragonfly"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Dragonfly</span></div>

			<div class="cldisp_e engineer"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Engineer</span></div>
			<div class="cldisp_e electron"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Electron</span></div>
			<div class="cldisp_e bastion"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Bastion</span></div>

			<div class="cldisp_r recon"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Recon</span></div>
			<div class="cldisp_r nighthawk"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Nighthawk</span></div>
			<div class="cldisp_r raptor"><div></div><span class="flvl" title="Level necessário">40</span><span class="frame">Raptor</span></div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="ad">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Tumblr 728 -->
	<ins class="adsbygoogle"
	     style="display:inline-block;width:728px;height:90px"
	     data-ad-client="ca-pub-4663805906186184"
	     data-ad-slot="6824271236"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	</div>
	<div class="clear"></div>
	<div id="board">
		<div class="wrapp">
	        <div class="title">
						Perks -
						<?php
							if(
								isset($_GET['lang']) && $_GET['lang'] == 'en' ||
								isset($_GET['lang']) && $_GET['lang'] == '' ||
								!isset($_GET['lang'])
							){
								echo '<a href="?lang=br">br</a>';
							}else{
								echo '<a href="?lang=en">en</a>';
							}
						?>
					</div>
	        <div id="todo">
			<?php
			if (isset($_GET['lang']) && $_GET['lang'] == 'br'):
				include("localization/br.php");
			else:
				include("localization/en.php");
			endif;

			$cperk;
			for($x = 0; $x < 105; $x++) {
				if ($x == 0):
					$cperk = "#item0";
				else:
					$cperk .= ",#item".$x;
				endif;

				if($x < 41 || $x == 100 || $x == 102 || $x == 104):
					echo "
						<div id='item".$x."' data-index='".$x."' data-weight='1' draggable='true' title='".$names[$x]."' alt='".$names[$x]."' class='tooltip'>
							<span class='picon'></span>
							<div class='tooltip-content'>
								<div class='tooltip-text'>
									<div class='tooltip-inner'>
										<span>".$names[$x]."</span> ".$desc[$x]."  <i>Lv1~10</i>
									</div>
								</div>
							</div>
						</div>
					";
				elseif (($x > 40 && $x < 68) || $x == 101):
					echo "
						<div id='item".$x."' data-index='".$x."' data-level='20' data-weight='2' draggable='true' title='".$names[$x]."' alt='".$names[$x]."' class='tooltip'>
							<span class='picon'></span>
							<div class='tooltip-content'>
								<div class='tooltip-text'>
									<div class='tooltip-inner'>
										<span>".$names[$x]."</span> ".$desc[$x]."  <i>Lv20</i>
									</div>
								</div>
							</div>
						</div>
					";
				elseif (($x > 67 && $x < 84) || $x == 103):
					echo "
						<div id='item".$x."' data-level='30' data-index='".$x."' data-weight='5' draggable='true' title='".$names[$x]."' alt='".$names[$x]."' class='tooltip'>
							<span class='picon'></span>
							<div class='tooltip-content'>
								<div class='tooltip-text'>
									<div class='tooltip-inner'>
										<span>".$names[$x]."</span> ".$desc[$x]."  <i>Lv30</i>
									</div>
								</div>
							</div>
						</div>
					";
				else:
					echo "
						<div id='item".$x."' data-level='40' data-index='".$x."' data-weight='8' draggable='true' title='".$names[$x]."' alt='".$names[$x]."' class='tooltip'>
							<span class='picon'></span>
							<div class='tooltip-content'>
								<div class='tooltip-text'>
									<div class='tooltip-inner'>
										<span>".$names[$x]."</span> ".$desc[$x]."  <i>Lv40</i>
									</div>
								</div>
							</div>
						</div>
					";
				endif;
			}
			?>
			<div class="clear"></div>
			</div>
		</div>

		<div class="wrappr">
			<div class="title">Pontos <div id="counter">0</div>/21</div>
			<div id="inprogress">

			</div>
			<button class="reset clear" onclick="location.reload()">Limpar</button>
			<div class="share">
				<div class="title">Compartilhe sua build!</div>
				<div class="fb-share-button" data-href="http://firefallbrasil.com.br/perk/" data-layout="button"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-via="brasilfirefall">Tweet</a>
				<input type="text" value="http://firefallbrasil.com.br/perk/"/>
			</div>
			<div class="clear"></div>
			<div class="cred tooltip">
				Desenvolvimento <a href="http://www.firefallbrasil.com.br" title="Firefall Brasil" target="_blank">Equipe Firefall Brasil</a>
				<div class='tooltip-content'>
					<div class='tooltip-text'>
						<div class='tooltip-inner'>
							<span><img src="img/hammer.png"/></span> Um oferecimento marretadas :)
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
var perk = "<?php echo $cperk ?>";
</script>
<script type="text/javascript" src="js/perks.js"></script>
<div id="fb-root"></div>
<script>
(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=496135807157865&version=v2.0";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
</body>
</html>
